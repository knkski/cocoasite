from locust import HttpLocust, TaskSet, task


class PublicTaskSet(TaskSet):
    @task
    def index(self):
        self.client.get("/programming/")


class PublicLocust(HttpLocust):
    task_set = PublicTaskSet
    min_wait = 5000
    max_wait = 15000
