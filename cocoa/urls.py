from django.conf.urls import include
from django.contrib import admin
from django.urls import path
from django.views.generic.base import RedirectView
from rest_framework import routers
from django.conf.urls.static import static
from django.conf import settings

from programming.api import EventViewSet
from registration.api import AttendeeViewSet, VolunteerViewSet
from website.api import TagViewSet, UserViewSet

admin.autodiscover()

router = routers.DefaultRouter()
router.register(r"events", EventViewSet, basename="events")
router.register(r"volunteers", VolunteerViewSet)
router.register(r"attendees", AttendeeViewSet)
router.register(r"user", UserViewSet, basename="user")
router.register(r"tags", TagViewSet, basename="tags")

urlpatterns = [
    path("favicon.ico", RedirectView.as_view(url="/static/favicon.ico", permanent=True)),
    path("api/v1/", include(router.urls)),
    path("manage/admin/", admin.site.urls),
    path("accounts/", include("allauth.urls")),
    path("programming/", include("programming.urls")),
    path("registration/", include("registration.urls")),
    path("manage/", include("website.urls")),
    path("", include("cms.urls")),
]
