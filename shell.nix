{ pkgs ? import <nixpkgs> { } }:
let
  cocoasite = pkgs.poetry2nix.mkPoetryEnv {
    python = pkgs.python310;
    projectDir = ./.;
    preferWheels = true;
    overrides = pkgs.poetry2nix.overrides.withDefaults (self: super: {
      pytest-django =
        super.pytest-django.overridePythonAttrs (old: { postPatch = ""; });
    });
  };
in cocoasite.env
