from django.contrib import admin
from django.db.models import Case, Count, IntegerField, Sum, TextField, When
from django.forms import ModelForm, Textarea
from taggit.forms import TagField
from taggit_labels.widgets import LabelWidget

from convention.models import Convention
from registration import models


class ParticipatingInline(admin.TabularInline):
    model = models.Attendee.participating.through
    verbose_name = "Participating Panel"
    verbose_name_plural = "Participating Panels"

    fields = (
        "event",
        "is_moderator",
        "confirmed",
        "notes",
        "get_start_time",
        "constance",
    )

    readonly_fields = ("get_start_time",)

    ordering = (
        "event__start_time",
        "event__name",
    )

    formfield_overrides = {
        TextField: {"widget": Textarea(attrs={"style": "height: 2.5em"})},
    }

    def get_start_time(self, obj):
        if obj.event:
            return obj.event.start_time.strftime("%Y-%m-%d %I:%M %p")

    get_start_time.admin_order_field = "start_time"
    get_start_time.short_description = "Start Time"


class AttendeeForm(ModelForm):
    tags = TagField(required=False, widget=LabelWidget)


@admin.register(models.Attendee)
class AttendeeAdmin(admin.ModelAdmin):
    form = AttendeeForm
    list_display = (
        "email",
        "first_name",
        "last_name",
        "display_name",
        "get_event_count",
        "get_moderating_count",
        "tag_list",
    )

    search_fields = (
        "email",
        "first_name",
        "last_name",
        "display_name",
    )

    list_filter = ("tags",)

    list_max_show_all = 500
    list_per_page = 200

    inlines = [
        ParticipatingInline,
    ]

    fieldsets = (
        (None, {"fields": (("first_name", "last_name"), "display_name", "email")}),
        ("Notes", {"fields": ("notes",)}),
        ("Tags", {"fields": ("tags",)}),
        ("Other", {"fields": ("convention",)}),
    )

    def get_changeform_initial_data(self, request):
        return {"convention": Convention.objects.get()}

    def get_queryset(self, request):
        return (
            super(AttendeeAdmin, self)
            .get_queryset(request)
            .prefetch_related("tags")
            .annotate(
                event_count=Count("participant"),
                moderating_count=Sum(
                    Case(
                        When(participant__is_moderator=True, then=1),
                        default=0,
                        output_field=IntegerField(),
                    )
                ),
            )
        )

    @staticmethod
    def tag_list(obj):
        return ", ".join(o.name for o in obj.tags.all())

    def get_event_count(self, obj):
        return obj.participant_set.count()

    get_event_count.admin_order_field = "event_count"
    get_event_count.short_description = "Event Count"

    def get_moderating_count(self, obj):
        return obj.participant_set.filter(is_moderator=True).count()

    get_moderating_count.admin_order_field = "moderating_count"
    get_moderating_count.short_description = "Moderating Count"
