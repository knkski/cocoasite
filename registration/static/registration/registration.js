// Registration Form

function mcRegister(e) {
  e.preventDefault();

  // If the Donate box is checked, advance to the appropriate xxD type.
  var ops = document.getElementsByTagName("option");
  for (var i = 0; i < ops.length; i++) {
    var val = ops[i].getAttribute("class");
    if (val == "donate_b") {
      ops[i].removeAttribute("class");
    }
  }

  var sel = document.getElementById("os0");
  var s0 = sel.selectedIndex;
  if ((s0 < 6) && (document.getElementById("reg_cb6").checked)) {
    s0 += 6;
    sel.selectedIndex = s0;
  }
/*
  var ops = document.getElementsByTagName("option");
  for (var i = 0; i < ops.length; i++) {
    var val = ops[i].getAttribute("value");
    if (ops[i].selected) alert(val + " selected");
  }
*/

  var os1 = "";
  var os2 = "";
  var os3 = "";
  os1  += ":F:" + document.getElementById("first_name").value;
  os1  += ":L:" + document.getElementById("last_name").value;
  os1  += ":N:" + document.getElementById("badge_name").value;
  os2  += ":A:" + document.getElementById("address1").value;
  os2  += ":B:" + document.getElementById("address2").value;
  os2  += ":C:" + document.getElementById("city").value;
  os2  += ":S:" + document.getElementById("state").value;
  os2  += ":Z:" + document.getElementById("zip").value;
  os2  += ":O:" + document.getElementById("country").value;
  os3  += ":E:" + document.getElementById("email").value;
  os3  += ":P:" + document.getElementById("phone").value;
  os3  += ":I:" + (document.getElementById("reg_cb1").checked ? "W" : "");
  os3  +=           (document.getElementById("reg_cb2").checked ? "V" : "");
  os3  +=           (document.getElementById("reg_cb3").checked ? "P" : "");
  os3  +=           (document.getElementById("reg_cb4").checked ? "F" : "");
  os3  +=           (document.getElementById("reg_cb5").checked ? "K" : "");
  os3  +=           (document.getElementById("reg_cb6").checked ? "D" : "");
  os3  += "\n\n";
  document.getElementById("on1").value = "1";
  document.getElementById("on2").value = "2";
  document.getElementById("on3").value = "3";
  document.getElementById("os1").value = os1;
  document.getElementById("os2").value = os2;
  document.getElementById("os3").value = os3;

// alert("registration not submitted -- please try again later");
document.getElementById('regform').submit();
}

function setupRegistration()
{
  if (!document.getElementById("regform")) {
    return;
  }
  // Hide all of the Donation drop down selections
  var ops = document.getElementsByTagName("option");
  for (var i = 0; i < ops.length; i++) {
    var val = ops[i].getAttribute("value");
    var len = val.length;
    if ((len > 0) && (val.slice(-1) == 'D'))
      ops[i].setAttribute("class", "donate_b");
  }

  document.getElementById("reg_cb6_p").setAttribute("class", "donate_b");

  // Expose the Donate checkbox
  document.getElementById("reg_cb6").removeAttribute("class");
  document.getElementById("reg_cb6_lab").setAttribute("class", "checkbox");

  $('#register_submit').on('click', mcRegister);
// alert("test mode - please try again later");
}

$(document).ready(setupRegistration);
