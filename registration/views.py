import logging

from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from cocoa import settings
from convention.models import Convention
import requests

log = logging.getLogger(__name__)


@csrf_exempt
def notify(request):
    convention = Convention.objects.get()
    constance = convention.current_constance()
    con_num = constance.name.split(" ")[-1]

    ack_url = "https://ipnpb.paypal.com/cgi-bin/webscr"
    response = requests.post(
        ack_url,
        data=f"cmd=_notify-validate&{request.body.decode('utf-8')}".encode("utf-8"),
    )
    host = request.get_host()
    xml = [
        ("conname", convention.name),
        ("connum", con_num),
        ("regstatus", response.content.decode("utf-8")),
        ("remoteaddr", request.META.get("REMOTE_ADDR", "UNKNOWN")),
    ]
    xml += [(k, item) for k, lst in request.POST.lists() for item in lst]
    strung = "\n".join(f"    <{k}>{v}</{k}>" for k, v in xml)

    message = f"""<?xml version="1.0" encoding="utf-8" ?>
<paypalreg>
{strung}
</paypalreg>
"""

    payer_email = request.POST.get("payer_email", "UNKNOWN EMAIL")
    subject = f"{constance.name} Registration from {payer_email}"
    email = EmailMessage(
        subject=subject,
        body=message,
        to=settings.REGISTRATION_EMAILS,
        reply_to=settings.REGISTRATION_EMAILS,
        headers={
            "MIME-Version": 1.0,
            "Content-Type": "application/xml; charset=UTF-8",
            "Content-Disposition": "inline",
            "Content-Transfer-Encoding": "8bit",
            "Content-Description": f"{constance.name} registration",
            f"X-{convention.name}": con_num,
        },
    )
    email.send(fail_silently=False)

    return HttpResponse("")
