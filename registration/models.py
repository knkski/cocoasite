from django.db import models
from taggit.managers import TaggableManager
from taggit.models import GenericUUIDTaggedItemBase, TaggedItemBase

from convention.models import BaseModel, Convention
from website.models import User


class UUIDTaggedItem(GenericUUIDTaggedItemBase, TaggedItemBase):
    class Meta:
        verbose_name = "Tag"
        verbose_name_plural = "Tags"


NULL_UUID = "00000000-0000-0000-0000-000000000000"


class Attendee(BaseModel):
    """Models a convention attendee.

    Separate from the `convention.user` model for situations such as manually importing
    registered users, and one login being able to register multiple people.
    """

    convention = models.ForeignKey(
        Convention, on_delete=models.CASCADE, blank=False, null=False, default=NULL_UUID
    )

    # Non-unique, to handle cases such as two family members sharing the same email
    email = models.EmailField(
        verbose_name="Email address",
        max_length=255,
        help_text="Email address of the attendee",
    )

    first_name = models.CharField(
        max_length=255,
        help_text="First name of the attendee",
    )

    last_name = models.CharField(
        max_length=255,
        help_text="Last name of the attendee",
    )

    display_name = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        help_text="Allows setting up a preferred display name when first name/last name doesn't"
        "cut it. Useful for mononyms.",
    )

    registrant = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)

    participating = models.ManyToManyField(
        "programming.Event",
        through="programming.Participant",
        related_name="attendees",
        blank=True,
    )

    volunteered = models.ManyToManyField(
        "programming.Event",
        blank=True,
        related_name="volunteered",
        through="Volunteer",
    )

    notes = models.TextField(
        default="",
        blank=True,
        null=False,
        help_text="Notes about this Attendee. Not shown publicly",
    )

    tags = TaggableManager(through=UUIDTaggedItem)

    class Meta:
        ordering = ["first_name", "last_name", "email"]
        unique_together = ("email", "first_name", "last_name")

    def __str__(self):
        return f"Attendee - {self.get_display_name()} ({self.email})"

    def get_display_name(self):
        return self.display_name or f"{self.first_name} {self.last_name}"


class Volunteer(BaseModel):
    event = models.ForeignKey(
        "programming.Event",
        on_delete=models.CASCADE,
        help_text="The event this user has volunteered for",
    )

    attendee = models.ForeignKey(
        Attendee,
        on_delete=models.CASCADE,
        help_text="The attendee that volunteered",
    )

    details = models.TextField(
        default="",
        blank=True,
        help_text="Attendee-provided details on why they volunteered",
    )

    class Meta:
        ordering = ["attendee__email"]

    def __str__(self):
        return f"<Volunteer: {self.attendee.email} -> {self.event.name}>"

    __repr__ = __str__
