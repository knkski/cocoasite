import textwrap

from django.core.mail import EmailMessage
from rest_framework import serializers, viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response

from cocoa import settings
from programming.api import EventViewSet

from . import models


class VolunteerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Volunteer
        fields = ("event_id", "attendee_id", "details")
        depth = 1


class VolunteerViewSet(viewsets.ModelViewSet):
    serializer_class = VolunteerSerializer
    queryset = models.Volunteer.objects.none()
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return models.Volunteer.objects.filter(attendee__registrant=self.request.user)

    def create(self, request, event_id=None, why=None, about=None):
        eid = request.data.get("event_id", event_id)
        why = request.data.get("why", why)
        about = request.data.get("about", about)
        event = EventViewSet().get_queryset().get(uuid=eid)
        attendee = request.user.attendee_set.get()

        if models.Volunteer.objects.filter(event=event, attendee=attendee).exists():
            raise PermissionDenied()

        volunteer = models.Volunteer(
            event=event,
            attendee=attendee,
            details=textwrap.dedent(
                f"""
                Why I'm volunteering:
                {why}
                About Me:
                {about}
                """
            ).strip(),
        )
        volunteer.save()

        if settings.PROGRAMMING_EMAILS:
            host = request.get_host()
            message = (
                f"The user {attendee.get_display_name()} ({attendee.email}) just volunteered "
                f"for the panel {event.name} (https://{host}/programming/{event.id}/)."
                f" They said this while volunteering:\n\nWhy I'm volunteering:\n\n{why}\n\n"
                f"About Me:\n\n{about}\n\n"
                "This volunteering request can be viewed here:\n\n"
                f"https://{host}/admin/programming/event/{event.id}/change/"
            )
            email = EmailMessage(
                subject="Panel Volunteer",
                body=message,
                to=settings.PROGRAMMING_EMAILS,
                reply_to=settings.PROGRAMMING_EMAILS,
            )
            email.send(fail_silently=False)

        return Response(VolunteerSerializer(volunteer).data)

    def list(self, request):
        volunteered = self.get_queryset()

        return Response(VolunteerSerializer(volunteered, many=True).data)


class AttendeeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Attendee
        fields = ("email", "first_name", "last_name")
        depth = 1


class AttendeeViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = AttendeeSerializer
    queryset = models.Attendee.objects.all()
    permission_classes = (IsAdminUser,)
