from django.http import Http404
from rest_framework import filters, mixins, serializers, viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from taggit.models import Tag

from programming.api import EventViewSet
from website import models


class UserSerializer(serializers.ModelSerializer):
    email = serializers.CharField(default=None)
    first_name = serializers.CharField(default=None)
    last_name = serializers.CharField(default=None)
    starred = serializers.SerializerMethodField()
    is_staff = serializers.BooleanField(read_only=True)

    def get_starred(self, user):
        try:
            starred = user.starred.all()
        except AttributeError:
            starred = []

        return [e.uuid for e in starred]

    class Meta:
        model = models.User
        fields = (
            "uuid",
            "email",
            "first_name",
            "last_name",
            "starred",
            "is_authenticated",
            "is_staff",
        )
        depth = 0


class UserViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
):

    queryset = models.User.objects.none()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        if self.request.user.is_authenticated:
            return self.request.user

        raise Http404("Unauthorized")

    def list(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request):
        if "starred" in request.data:
            starred = request.data["starred"]
            request.user.starred.set(
                EventViewSet().get_queryset().filter(visible=True, uuid__in=starred)
            )

        return Response(self.serializer_class(request.user).data)


class TagSerializer(serializers.ModelSerializer):
    name = serializers.CharField()
    slug = serializers.CharField()

    class Meta:
        model = Tag
        fields = ("id", "name", "slug")


class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    permission_classes = (IsAuthenticated,)
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ("name", "slug")
