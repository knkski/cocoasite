from django import forms
from django.core.exceptions import SuspiciousOperation

from convention.models import Convention
from registration.models import Attendee
from website.models import User


class SignupForm(forms.ModelForm):
    class Meta:
        model = User
        fields = (
            "email",
            "first_name",
            "last_name",
        )

    def signup(self, request, user):
        convention = Convention.objects.get()
        constance = convention.current_constance()
        # Something weird is going on if they already have an attendee associated with them
        if user.attendee_set.exists():
            raise SuspiciousOperation(
                f"Somehow, the user {user} with an existing attendee set signed up!"
            )

        # Otherwise, they might have an unassociated Attendee that was manually added, so check
        # to see if one exists here.
        try:
            attendee = Attendee.objects.get(
                email=user.email,
                first_name=user.first_name,
                last_name=user.last_name,
                registrant=None,
                convention=convention,
            )
        except Attendee.DoesNotExist:
            attendee = Attendee(
                email=user.email,
                first_name=user.first_name,
                last_name=user.last_name,
                registrant=user,
                convention=convention,
            )

        attendee.save()
