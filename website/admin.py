from django.contrib import admin

from registration.models import Attendee

from . import models


class AttendeesInline(admin.TabularInline):
    model = Attendee


@admin.register(models.User)
class UserAdmin(admin.ModelAdmin):
    list_display = (
        "email",
        "first_name",
        "last_name",
        "is_active",
        "is_staff",
        "is_superuser",
    )

    search_fields = (
        "email",
        "first_name",
        "last_name",
    )

    filter_horizontal = ("user_permissions",)

    list_filter = (
        "is_active",
        "is_staff",
        "is_superuser",
    )

    fieldsets = (
        (None, {"fields": (("first_name", "last_name"), "email")}),
        ("Access", {"fields": (("is_active", "is_staff"),)}),
        ("Permissions", {"fields": ("user_permissions",)}),
        ("Miscellaneous", {"fields": ("last_login",)}),
    )

    def activate_users(self, _, queryset):
        queryset.update(is_active=True)

    activate_users.short_description = "Mark selected users as active"

    def deactivate_users(self, _, queryset):
        queryset.update(is_active=False)

    deactivate_users.short_description = "Mark selected users as inactive"

    actions = [activate_users, deactivate_users]

    inlines = [
        AttendeesInline,
    ]
