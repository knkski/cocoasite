from django.urls import path

from . import views

urlpatterns = [
    path("reports/tent_cards/", views.tent_cards, name="reports_tent_cards"),
    path("reports/door_cards/", views.door_cards, name="reports_door_cards"),
    path("reports/badge_labels/", views.badge_labels, name="reports_badge_labels"),
    path("reports/pocket_program/", views.pocket_program, name="reports_pocket_program"),
    path("reports/programbook/", views.program_book, name="reports_program_book"),
    path("reports/", views.reports_index, name="reports_index"),
    path("email/", views.email_index, name="email_index"),
]
