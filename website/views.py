import json
from collections import OrderedDict
from itertools import groupby

import arrow
from django.contrib.admin.views.decorators import staff_member_required
from django.db.models import DateTimeField
from django.db.models.functions import Trunc
from django.shortcuts import redirect, render
from rest_framework.renderers import JSONRenderer
from taggit.models import Tag

from convention.models import Convention
from programming import api
from programming.models import Participant
from registration.models import Attendee
from website.api import TagSerializer

BADGE_LABELS_SQL = """
SELECT DISTINCT
  "registration_attendee"."id",
  "registration_attendee"."first_name",
  "registration_attendee"."last_name",
  "registration_attendee"."display_name",
  "programming_event"."start_time",
  "programming_event"."name" AS "event_name",
  "programming_room"."short_name" AS "room_name"
FROM
  "registration_attendee"
  INNER JOIN "programming_participant" ON (
    "registration_attendee"."id" = "programming_participant"."attendee_id"
  )
  INNER JOIN "programming_event" ON (
    "programming_participant"."event_id" = "programming_event"."id"
  )
  INNER JOIN "programming_room" ON (
    "programming_event"."room_id" = "programming_room"."id"
  )
WHERE
  (
    "programming_participant"."confirmed" = True
    AND "programming_event"."constance_id" = %s
    AND "programming_event"."room_id" IS NOT NULL
    AND "programming_event"."start_time" IS NOT NULL
    AND "programming_participant"."id" IS NOT NULL
  )
ORDER BY
  "registration_attendee"."first_name" ASC;
""".strip()


@staff_member_required
def reports_index(request):
    events = (
        api.EventViewSet()
        .get_queryset()
        .filter(room__isnull=False, start_time__isnull=False)
        .order_by("start_time")
    )

    events = (
        JSONRenderer()
        .render(api.EventSerializer(events, many=True, context={"request": request}).data)
        .decode("utf-8")
    )

    participant_emails = json.dumps(
        {
            f"{p.attendee.get_display_name()}": p.attendee.email
            for p in Participant.objects.select_related("attendee")
        }
    )

    return render(request, "website/reports/index.html", locals())


@staff_member_required
def email_index(request):
    return render(request, "website/email/index.html", locals())


@staff_member_required
def program_book(request):
    events = api.EventViewSet().get_queryset()

    events = (
        JSONRenderer()
        .render(api.EventSerializer(events, many=True, context={"request": request}).data)
        .decode("utf-8")
    )

    return render(request, "website/reports/program_book.html", locals())


@staff_member_required
def tent_cards(request):
    events = (
        api.EventViewSet()
        .get_queryset()
        .filter(room__isnull=False, start_time__isnull=False)
        .order_by(
            Trunc("start_time", "day", output_field=DateTimeField()).asc(),
            "room",
            "start_time",
        )
    )

    return render(request, "website/reports/tent_cards.html", {"events": events})


@staff_member_required
def door_cards(request):
    constance = Convention.objects.get().current_constance()
    events = (
        api.EventViewSet()
        .get_queryset()
        .filter(start_time__isnull=False, visible=True, room__isnull=False)
    )
    dates = sorted({e.start_time.strftime("%Y-%m-%d") for e in events})

    rooms = constance.room_set.exclude(name="Other").order_by("name")

    days = OrderedDict(
        (
            arrow.get(day),
            {
                room.name: room.get_active_events(day)
                for room in rooms
                if room.get_active_events(day).count()
            },
        )
        for day in dates
    )
    return render(request, "website/reports/door_cards.html", {"days": days})


@staff_member_required
def badge_labels(request):
    constance = Convention.objects.get().current_constance()

    attendees = Attendee.objects.raw(BADGE_LABELS_SQL, [constance.id])
    attendees = [(att, list(group)) for att, group in groupby(attendees)]

    chunked = [attendees[i : i + 8] for i in range(0, len(attendees), 8)]

    return render(request, "website/reports/badge_labels.html", {"chunked": chunked})


@staff_member_required
def pocket_program(request):
    constance = Convention.objects.get().current_constance()
    events = api.EventViewSet().get_queryset().filter(room__isnull=False, start_time__isnull=False)
    rooms = constance.room_set.all().order_by("name")
    important_ids = [str(r.id) for r in constance.room_set.filter(important=True)]

    events = (
        JSONRenderer()
        .render(api.EventSerializer(events, many=True, context={"request": request}).data)
        .decode("utf-8")
    )

    rooms_serialized = (
        JSONRenderer()
        .render(api.RoomSerializer(rooms, many=True, context={"request": request}).data)
        .decode("utf-8")
    )

    return render(request, "website/reports/pocket_program.html", locals())
