$(function() {
  let event_to_string = function(e) {
    return [
      moment.utc(e.start_time).format('dddd h:mm A'),
      e.name,
      e.description,
      e.room.name,
      e.participants.map((p) => p.name + (p.is_moderator ? ' (m)' : '')).join(', '),
    ].join("\n").replace(/\n+/g, '\n')
  };

  $("#email-report").on("mousedown", function(e) {
    e.preventDefault();
    e.stopPropagation();

    let emails = new Map();

    window.events.forEach(function(e) {
      e.participants.forEach(function(p) {
        let list = emails.get(p.name) || [];

        list.push(event_to_string(e));

        emails.set(p.name, list);
      })
    });

    let entries = [...emails.entries()];
    entries.sort();

    let file_contents = entries.reduce(function(memo, e) {
      return memo + e[0] + ' ' + window.participant_emails[e[0]] + '\n\n' + e[1].join('\n\n') + '\n\n---\n\n';
    }, '')
    .replace(/[‘’“”''""]/g, "'");

    let $link = $(e.target).closest("a");

    $link.attr("href", "data:application/octet-stream;charset=utf-8;base64," + btoa(file_contents));
    $link.attr("download", "emails.txt");
  });
});
