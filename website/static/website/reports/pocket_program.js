// Gets the start and end time for the particular day
let get_start_end = function(events) {
  let start_time = events.reduce(function(memo, e) {
    let st = moment.utc(e.start_time).startOf('hour');

    return memo === null ?
      st :
      (st < memo) ? st : memo;
  }, null).format("HH:mm:ss");

  let end_time = events.reduce(function(memo, e) {
    let et = moment.utc(e.start_time).add(e.duration, 'minutes');

    return (memo === null) ?
      et :
      (et > memo) ? et : memo;
  }, null).format("HH:mm:ss");

  // If the end time is something like 02:00, we're displaying post-midnight times
  if (end_time < start_time) {
    end_time = '1.' + end_time;
  }

  return [start_time, end_time];
};

$(function() {
  // Add in moment range plugin
  window['moment-range'].extendMoment(moment);

  // Collapse unimportant rooms into "Other"
  window.rooms = window.rooms.filter((r) => window.important_ids.includes(r.id));
  window.rooms.push({id: 'other', name: 'Other', color: '#AAA'});

  window.events.forEach((e) => {
    if (!window.important_ids.includes(e.room.id)) {
      e.room = {
        id: 'other',
        name: 'Other',
        color: e.room.color,
      }
    }
  });

  // Calculate the list of days
  let days = window.events.reduce((memo, e) => {
    memo.add(moment.utc(e.start_time).format('YYYY-MM-DD'));
    return memo;
  }, new Set());

  // For each day, add a calendar of that day's activities
  Array.from(days).sort().forEach((day, i) => {
    let $calendar = $(`<div id='calendar-${i}' class="calendar"></div>`);
    $("body").append($calendar);

    let events = window.events.filter((e) => moment.utc(e.start_time).add(-6, 'hours').format('YYYY-MM-DD') === day);

    let [start_time, end_time] = get_start_end(events);

    $calendar.fullCalendar({
      schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
      allDaySlot: false,
      defaultView: 'agendaDay',
      defaultDate: day,
      editable: false,
      selectable: false,
      clickable: false,
      disableResizing: true,
      eventLimit: false,
      height: "auto",
      header: false,
      minTime: start_time,
      maxTime: end_time,
      viewRender (){
        // Insert short day name into upper right empty slot
        $calendar.find(".fc-axis.fc-widget-header").text(moment.utc(day).format("ddd"))
      },
      resources: window.rooms.map((r) => ({
        id: r.id,
        title: r.name,
        color: r.color
      })),
      events: events.map((e) => ({
        id: e.id,
        resourceId: e.room.id,
        start: e.start_time,
        end: moment.utc(e.start_time).add(e.duration, 'minutes'),
        title: e.name,
      })),
    });

    $calendar.fullCalendar('render');
  });
});