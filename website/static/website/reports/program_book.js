$(function() {
  "use strict";

  let app = new Vue({
    data: {
      events: window.events,
      event_name: window.event_name,
    },
    computed: {
      dates: function() {
        return _.chain(this.events)
          .groupBy(event =>
            (event.start_time == null) ?
              'unset' :
              moment.utc(event.start_time).format('YYYY-MM-DD'))
          .mapValues(events =>
            _.chain(events)
            .groupBy(event =>
              (event.start_time == null) ?
                'unset' :
                moment.utc(event.start_time).format('HH:mm'))
            .mapValues(times =>
              _.groupBy(times, app.get_room_name))
            // Convert object to list of key/value pairs, sort the list
            // by key, and then convert to just a list of values.
            .entries()
            .sortBy((e) => e[0])
            .map((e) => e[1])
            .value())
          .entries()
          // Always sort unscheduled events last
          .sortBy((e) => e[0] === 'unset' ? '9999-99-99' : e[0])
          .value();
      }
    },
    methods: {
      pretty_date(date) {
        return (date == null) ? '' : moment.utc(date).format('HH:mm');
      },
      get_day_of_week(date) {
        return (date === 'unset') ? 'Not yet scheduled' : moment.utc(date).format('dddd');
      },
      get_room_name(event) {
        return event.room == null ? 'Room Not Set' : event.room.name;
      }
    },
  });

  app.$mount('#content');
});