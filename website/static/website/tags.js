$(function() {
  Vue.use(Vuetable);
  Vue.use(VueResource);

  Vue.http.interceptors.push(function(request) {
    request.headers.set('X-CSRFToken', window.csrf_token);
  });

  let API = "/api/v1/tags/";

  let slugify = (name) => name.toString().toLowerCase()
    .replace(/\s+/g, '-')           // Replace spaces with -
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, ''); // Trim - from end of text

  var app = new Vue({
    el: "#tags",
    delimiters: ['[[', ']]'],
    data: {
      fields: [{
        name: "name",
        sortField: "name",
      }, {
        name: "slug",
        sortField: "slug",
      }, {
        name: '__slot:actions',
        title: 'Actions',
        titleClass: 'text-center',
        dataClass: 'text-center',
      }],
      css: {
        table: {
          tableClass: 'table table-striped table-bordered table-hovered',
        }
      },
      sortOrder: [
        { field: 'name', direction: 'asc' }
      ],
      error_response: '',
      new_tag_name: '',
      loading: true,
    },
    methods: {
      add() {
        this.$http
        .post(API, {name: this.new_tag_name, slug: slugify(this.new_tag_name)})
        .then((response) => {
          this.new_tag_name = '';
          this.$refs.vuetable.refresh();
        })
        .catch(() => alert("Sorry, the tag could not be created. Please try again later."))
      },
      remove (rowData) {
        this.$http
        .delete(API + rowData.id + '/')
        .then((response) => {
          this.$refs.vuetable.refresh();
        })
        .catch(() => alert("Sorry, the tag could not be deleted. Please try again later."))
      },
      onLoading() {
        this.loading = true;
      },
      onLoaded() {
        this.loading = false;
      },
      makeQueryParams (sortOrder, currentPage, perPage) {
        return {
          ordering: sortOrder.map(function(sort) {
            return (sort.direction === 'desc' ? '-' : '') + sort.field
          }).join(','),
        }
      },
      onPaginationData (paginationData) {
        this.$refs.pagination.setPaginationData(paginationData)
      },
      onChangePage (page) {
        this.$refs.vuetable.changePage(page)
      },
    }
  })

  // mount
  app.$mount()
});
