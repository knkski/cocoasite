from datetime import datetime

from django.db.models import Prefetch, QuerySet
from rest_framework import mixins, serializers, viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from cocoa import settings
from convention.models import Constance, Convention

from . import models


class RoomSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Room
        fields = ("id", "name", "color")
        extra_kwargs = {"id": {"read_only": False, "required": True}}


class FormatSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Format
        fields = ("name", "color")


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Category
        fields = ("name", "color")


class ParticipantSerializer(serializers.HyperlinkedModelSerializer):
    user_id = serializers.ReadOnlyField(source="attendee__id")
    is_moderator = serializers.BooleanField()
    name = serializers.ReadOnlyField(source="attendee.get_display_name")

    class Meta:
        model = models.Participant
        fields = ("user_id", "is_moderator", "name")


class EventSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.CharField(read_only=True, source="uuid")
    room = RoomSerializer()
    format = FormatSerializer(read_only=True)
    category = CategorySerializer(read_only=True)

    # We use the unfiltered `participant_set` here, since the `confirmed=True` filtering happens in
    # the `prefetch_related` bits down below in `EventSerializer`.
    participants = ParticipantSerializer(read_only=True, many=True, source="participant_set")

    class Meta:
        model = models.Event
        fields = (
            "id",
            "name",
            "description",
            "start_time",
            "duration",
            "room",
            "format",
            "category",
            "participants",
        )
        depth = 1


class EventViewSet(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
):
    def get_queryset(self) -> QuerySet:
        # Do all of the work possible up-front. FKs on the Event table can use `select_related`, but
        # the M2M relationships have to use a more complex `prefetch_related` that does the
        # filtering
        con = Convention.objects.get()
        return (
            con.current_constance()
            .event_set.filter(visible=True)
            .select_related("room", "format", "category")
            .prefetch_related(
                Prefetch(
                    "participant_set",
                    queryset=models.Participant.objects.filter(confirmed=True).select_related(
                        "attendee"
                    ),
                )
            )
        )

    serializer_class = EventSerializer
    permission_classes = (IsAuthenticated,)

    def get_renderer_context(self):
        context = super().get_renderer_context()

        user = context["request"].user
        if user.is_authenticated and user.email in settings.TSV_USERS:
            context["writer_opts"] = {"delimiter": "\t"}
        return context

    def create(self, request, *args, **kwargs):
        event = models.Event(
            name=request.data["title"],
            description=request.data["description"],
            notes=f"Suggested by {request.user.email} at {datetime.now()}",
            proposer=request.user,
            visible=False,
            constance=Constance.objects.get(),
            # Since it defaults to convention start time, for the admin site
            start_time=None,
        )

        event.save()

        return Response(EventSerializer(event).data)

    def update(self, request, *args, **kwargs):
        if not self.request.user.is_superuser:
            raise PermissionDenied()

        event = self.get_queryset().get(pk=kwargs["pk"])

        # Handle moving about the calendar
        if "start_time" in request.data:
            event.start_time = request.data["start_time"]
            event.room = models.Room.objects.get(id=request.data["room"]["id"])
            event.save()

        # Handle changing the event duration
        if "duration" in request.data:
            event.duration = int(request.data["duration"])
            event.save()

        return Response(EventSerializer(event).data)
