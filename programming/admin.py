from django.contrib import admin
from django.db.models import Count, TextField
from django.forms import Textarea

from datetime import date, timedelta
from convention.models import Convention
from registration.models import Volunteer

from . import models


class OldConstanceMixin:
    """Ensures things associated with old constances are readonly."""

    def has_change_permission(self, request, obj=None):
        if obj:
            constance = Convention.objects.get().current_constance()
            if date.today() <= obj.constance.end_date + timedelta(days=3):
                return super().has_change_permission(request, obj)

        return False

    def has_delete_permission(self, request, obj=None):
        if obj:
            constance = Convention.objects.get().current_constance()
            if date.today() <= obj.constance.end_date + timedelta(days=3):
                return super().has_delete_permission(request, obj)

        return False


class ParticipantsInline(admin.TabularInline):
    model = models.Event.participants.through

    formfield_overrides = {
        TextField: {"widget": Textarea(attrs={"style": "height: 2.5em"})},
    }


class VolunteersInline(admin.TabularInline):
    model = Volunteer

    readonly_fields = (
        "attendee",
        "details",
    )


@admin.register(models.Room)
class RoomAdmin(OldConstanceMixin, admin.ModelAdmin):
    list_display = ("name", "short_name", "color", "important", "constance")
    list_filter = ("important", "constance")
    search_fields = ("name", "short_name")
    ordering = ("constance", "name")

    fieldsets = (
        (
            None,
            {
                "fields": (("name", "short_name"), "important", "color", "constance"),
            },
        ),
    )

    def get_changeform_initial_data(self, request):
        return {"constance": Convention.objects.get().current_constance()}


@admin.register(models.Format)
class FormatAdmin(OldConstanceMixin, admin.ModelAdmin):
    list_display = ("name", "color", "constance")
    search_fields = ("name",)
    ordering = ("constance", "name")
    list_filter = ("constance",)

    fieldsets = ((None, {"fields": ("name", "color", "constance")}),)

    def get_changeform_initial_data(self, request):
        return {"constance": Convention.objects.get().current_constance()}


@admin.register(models.Category)
class CategoryAdmin(OldConstanceMixin, admin.ModelAdmin):
    list_display = ("name", "color", "constance")
    search_fields = ("name",)
    ordering = ("constance", "name")
    list_filter = ("constance",)

    fieldsets = ((None, {"fields": ("name", "color", "constance")}),)

    def get_changeform_initial_data(self, request):
        return {"constance": Convention.objects.get().current_constance()}


@admin.register(models.Event)
class EventAdmin(OldConstanceMixin, admin.ModelAdmin):
    list_display = (
        "name",
        "visible",
        "get_room",
        "get_format",
        "get_category",
        "start_time",
        "attendance_size",
        "participant_count",
        "get_constance",
    )

    def wrapper(title):
        class Wrapper(admin.FieldListFilter):
            def __new__(cls, *args, **kwargs):
                instance = admin.FieldListFilter.create(*args, **kwargs)
                instance.title = title
                return instance

        return Wrapper

    list_filter = (
        ("constance__name", wrapper("Constance")),
        "visible",
        "room",
        "format",
        "category",
        "start_time",
    )

    search_fields = (
        "name",
        "description",
        "notes",
    )

    list_max_show_all = 500
    list_per_page = 200

    def get_changeform_initial_data(self, request):
        return {"constance": Convention.objects.get().current_constance()}

    def get_room(self, obj):
        if obj.room:
            return obj.room.name

    get_room.admin_order_field = "room"
    get_room.short_description = "Room"

    def get_constance(self, obj):
        return obj.constance.name

    get_constance.admin_order_field = "constance"
    get_constance.short_description = "Constance"

    def get_format(self, obj):
        if obj.format:
            return obj.format.name

    get_format.admin_order_field = "format"
    get_format.short_description = "Format"

    def get_category(self, obj):
        if obj.category:
            return obj.category.name

    get_category.admin_order_field = "category"
    get_category.short_description = "Category"

    def get_constance(self, obj):
        return obj.constance.name

    get_constance.admin_order_field = "constance"
    get_constance.short_description = "Constance"

    def get_queryset(self, request):
        return (
            super(EventAdmin, self)
            .get_queryset(request)
            .annotate(participant_count=Count("participants"))
        )

    def participant_count(self, obj):
        return obj.participant_count

    participant_count.admin_order_field = "participant_count"
    participant_count.short_description = "Participant Count"

    inlines = [
        ParticipantsInline,
        VolunteersInline,
    ]

    fieldsets = (
        (None, {"fields": ("name", "description", "proposer", "visible")}),
        ("Format & Category", {"fields": (("format", "category"),)}),
        ("Time & Location", {"fields": (("start_time", "duration"), "room")}),
        ("Notes", {"fields": ("notes", "attendance_size")}),
        ("Other", {"fields": ("constance",)}),
    )

    def see_event(self, _, queryset):
        queryset.update(visible=True)

    see_event.short_description = "Mark selected events as visible"

    def unsee_event(self, _, queryset):
        queryset.update(visible=False)

    unsee_event.short_description = "Mark selected events as not visible"

    actions = [see_event, unsee_event]
