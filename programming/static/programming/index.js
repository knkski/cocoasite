$(function() {
  $(".toggle-expansion").show().on("click", function() {
    let $this = $(this);

    if ($this.hasClass("expanded")) {
      $this.removeClass("expanded");
      $(".event-rows").removeClass("expanded");
      $this.find("span").attr("class", "glyphicon glyphicon-resize-full");
    } else {
      $this.addClass("expanded");
      $(".event-rows").addClass("expanded");
      $this.find("span").attr("class", "glyphicon glyphicon-resize-small");
    }
  })
})
