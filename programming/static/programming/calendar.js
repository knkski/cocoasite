// Gets the start and end time for the particular day
let get_start_end = function(events) {
  let start_time = events.reduce(function(memo, e) {
    let st = moment.utc(e.start_time).add(-30, 'minutes').startOf('hour');

    return memo === null ?
      st :
      (st < memo) ? st : memo;
  }, null).format("HH:mm:ss");

  let end_time = events.reduce(function(memo, e) {
    let et = moment.utc(e.start_time).add(e.duration + 30, 'minutes');

    return (memo === null) ?
      et :
      (et > memo) ? et : memo;
  }, null).format("HH:mm:ss");

  // If the end time is something like 02:00, we're displaying post-midnight times
  if (end_time < start_time) {
    end_time = '1.' + end_time;
  }

  return [start_time, end_time];
};

$(function() {
  if (!window.events.length) {
    return;
  }
  let day_start = moment.duration(window.day_start);

  // Add in moment range plugin
  window['moment-range'].extendMoment(moment);

  // Keep track of removed rooms for the reset button to work its magic
  let removed_rooms = [];

  let range = moment.range(
    moment.utc(window.first_date).startOf('day').add(day_start),
    moment.utc(window.first_date).endOf('day').add(day_start),
  );

  let events = window.events.filter((e) => range.contains(moment.utc(e.start_time)));

  let [start_time, end_time] = get_start_end(events);

  let convention_start = window.events
    .reduce((memo, e) => {
      let st = moment.utc(e.start_time);

      return memo === null ?
        st :
        memo < st ? memo : st;
    }, null).startOf('day');

  let convention_end = window.events
    .reduce((memo, e) => {
      let st = moment.utc(e.start_time);

      return memo === null ?
        st :
        memo > st ? memo : st;
    }, null).endOf('day');

  let $calendar = $("#calendar");

  $calendar.fullCalendar({
    schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
    allDaySlot: false,
    defaultView: 'agendaDay',
    defaultDate: window.first_date,
    editable: window.is_staff,
    selectable: false,
    eventLimit: false,
    height: "parent",
    header: {
      left: 'prev,next reset',
      center: 'title',
      //right: 'agendaDay,agendaTwoDay'
    },
    validRange: {
      start: convention_start,
      end: convention_end,
    },
    minTime: start_time,
    maxTime: end_time,
    customButtons: {
      reset: {
        text: 'Show All Rooms',
        click: function() {
          removed_rooms.forEach((room_id) => {
            let room = window.rooms.find((r) => r.id === room_id);

            $calendar.fullCalendar('addResource', {
              id: room_id,
              title: room.name,
              color: room.color
            }, false);
          });

          removed_rooms = [];
        }
      }
    },
    resourceRender: function(resourceObj, labelTds) {
      labelTds.on('click', function(e){
        let room_id = $(e.currentTarget).data('resource-id');

        $calendar.fullCalendar('removeResource', room_id);
        removed_rooms.push(room_id);
      });
    },
    viewDestroy(view, el) {
      let range = moment.range(
        moment(view.intervalStart).add(day_start),
        moment(view.intervalEnd).add(day_start),
      );

      let events = window.events.filter((e) => range.contains(moment.utc(e.start_time)));

      let [start_time, end_time] = get_start_end(events);

      let current_start_time = $calendar.fullCalendar('option', 'minTime');
      let current_end_time = $calendar.fullCalendar('option', 'maxTime');

      if (start_time !== current_start_time || end_time !== current_end_time) {
        $calendar.fullCalendar('option', {
          'minTime': start_time,
          'maxTime': end_time,
        });
      }
    },
    views: {
      agendaTwoDay: {
        duration: {days: 2},
        groupByDateAndResource: true,
        groupByResource: true,
        type: 'agenda',
      }
    },

    resources: window.rooms.map((r) => ({
      id: r.id,
      title: r.name,
      color: r.color
    })),

    events: window.events.map((e) => ({
      id: e.id,
      resourceId: e.room.id,
      start: e.start_time,
      end: moment.utc(e.start_time).add(e.duration, 'minutes'),
      title: e.name,
      url: `/programming/${e.id}/`
    })),

    select(start, end, jsEvent, view, resource) {
      console.log(
        'select',
        start.format(),
        end.format(),
        resource ? resource.id : '(no resource)'
      );
    },
    eventMouseover(calEvent, jsEvent) {
      let event = window.events.find((e) => e.id === calEvent.id);
      let participants = event.participants.map((p) =>
        p.name + (p.is_moderator ? ' (m)' : '')
      ).join('<br />');

      if (participants === '') {
        participants = "No participants selected yet."
      }

      let tooltip = `<div class="tooltipevent" style="background-color: ${event.room.color}">` +
        `<strong>${event.name}</strong><br />${participants}` +
        `</div>`;

      let $tooltip = $(tooltip).appendTo('body');

      $(this).mouseover(function(e) {
        $(this).css('z-index', 10000);
        $tooltip.fadeIn('500');
        $tooltip.fadeTo('10', 1.9);
      }).mousemove(function(e) {
        $tooltip.css('top', e.pageY - 30);
        $tooltip.css('left', e.pageX + 20);
      });
    },

    eventMouseout(calEvent, jsEvent) {
      $(this).css('z-index', 8);
      $('.tooltipevent').remove();
    },

    eventClick(event) {
      if (event.url) {
        window.location.href = event.url;
        return false;
      }
    },

    eventResize(calEvent, delta, revertFunc, jsEvent, ui, view) {
      let event = window.events.find((e) => e.id === calEvent.id);

      return fetch(`/api/v1/events/${event.id}/`, {
          method: "PATCH",
          headers: {
            "Content-Type": "application/json",
            "X-CSRFToken": window.csrf_token,
          },
          body: JSON.stringify({
            duration: event.duration + delta.asMinutes()
          }),
          credentials: 'same-origin',
        })
        .then((response) => response.json())
        .then((data) => {
          calEvent.duration = data.duration;
          $calendar.fullCalendar('updateEvent', calEvent);
        });
    },

    eventDrop(calEvent, delta, revertFunc, jsEvent, ui, view) {
      let event = window.events.find((e) => e.id === calEvent.id);
      let room = window.rooms.find((r) => r.id === calEvent.resourceId);

      return fetch(`/api/v1/events/${event.id}/`, {
          method: "PATCH",
          headers: {
            "Content-Type": "application/json",
            "X-CSRFToken": window.csrf_token,
          },
          body: JSON.stringify({
            room: room,
            start_time: calEvent.start.toISOString(),
          }),
          credentials: 'same-origin',
        })
        .then((response) => response.json())
        .then((data) => {
          calEvent.resourceId = data.room.id;
          calEvent.start = moment.utc(data.start_time);
          calEvent.end = calEvent.start + moment.duration(data.duration, 'minutes');
          $calendar.fullCalendar('updateEvent', calEvent);
        });
    }
  });

  $calendar.fullCalendar('render');
});
