import json
from itertools import groupby

from django.db.models import Q
from django.db.models.functions import TruncDate, TruncMinute
from django.http import HttpResponse
from django.shortcuts import redirect, render
from rest_framework.exceptions import PermissionDenied
from rest_framework.parsers import FormParser
from rest_framework.renderers import JSONRenderer
from rest_framework.request import Request

from convention.models import Convention
from registration.api import VolunteerViewSet
from registration.models import Attendee, Volunteer
from .forms import NameForm
from . import api


def index(request):
    constance = Convention.objects.get().current_constance()
    events = (
        api.EventViewSet()
        .get_queryset()
        .annotate(date=TruncDate("start_time"), time=TruncMinute("start_time"))
        .order_by("date", "time")
    )

    formats = constance.format_set.all()
    categories = constance.category_set.all()
    rooms = constance.room_set.all()

    args = dict(request.GET.lists())
    try:
        search = args["search"][0]
    except (KeyError, IndexError):
        search = None

    if search:
        events = events.filter(
            Q(name__icontains=search)
            | Q(description__icontains=search)
            | Q(participant__attendee__first_name__icontains=search)
            | Q(participant__attendee__last_name__icontains=search)
            | Q(participant__attendee__display_name__icontains=search)
        ).distinct()

    for filt in ("format", "category", "room"):
        if value := [v for v in args.get(filt, []) if v]:
            events = events.filter(**{f"{filt}__uuid__in": value})

    grouped_events = [
        (
            date,
            [(time, list(events)) for time, events in groupby(by_date, lambda x: x.time)],
        )
        for date, by_date in groupby(events, lambda x: x.date)
    ]

    return render(request, "programming/index.html", locals())


def details(request, event_id):
    event = api.EventViewSet().get_queryset().get(uuid=event_id)

    if request.user.is_authenticated:
        is_volunteered = Volunteer.objects.filter(
            attendee__registrant=request.user, event__uuid=event_id
        ).exists()
        is_attending = Attendee.objects.filter(registrant=request.user).exists()
    else:
        is_volunteered = False
        is_attending = False

    return render(request, "programming/event_details.html", locals())


def suggest(request):
    if request.method == "POST":
        form = NameForm(request.POST)
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect("/thanks/")
        else:
            return redirect(f"/programming/suggest/")
    else:
        form = NameForm()
        return render(request, "programming/suggest.html", locals())


def volunteer(request, event_id):
    convention = Convention.objects.get()
    event = api.EventViewSet().get_queryset().get(uuid=event_id)

    if request.method == "POST":
        wrapped = Request(request, parsers=[FormParser])
        wrapped.user = request.user

        try:
            VolunteerViewSet().create(wrapped, event_id, request.POST["why"], request.POST["about"])
        except PermissionDenied:
            pass

        return redirect(f"/programming/{event.uuid}/")
    else:
        if request.user.is_authenticated:
            is_volunteered = Volunteer.objects.filter(
                attendee__registrant=request.user, event__uuid=event_id
            ).exists()
            is_attending = convention.attendee_set.filter(registrant=request.user).exists()
        else:
            is_volunteered = False
            is_attending = False

        return render(request, "programming/volunteer.html", locals())


def calendar(request):
    constance = Convention.objects.get().current_constance()
    events = api.EventViewSet().get_queryset().filter(room__isnull=False, start_time__isnull=False)
    rooms = constance.room_set.all().order_by("name")

    if events.exists():
        first_date = events.order_by("start_time")[0].start_time.strftime("%Y-%m-%d")
    else:
        first_date = None

    events = api.EventSerializer(events, many=True, context={"request": request}).data

    rooms = api.RoomSerializer(rooms, many=True, context={"request": request}).data

    day_start = Convention.objects.get().current_constance().day_start

    return render(request, "programming/calendar.html", locals())


def text(request):
    if not request.user.is_staff:
        return redirect("/programming/")

    visible = request.GET.get("visible")

    events = (
        api.EventViewSet()
        .get_queryset()
        .filter(room__isnull=False, start_time__isnull=False)
        .exclude(category__name__in=["Media", "Kids", "Music"])
        .order_by("start_time")
    )

    if visible:
        events = events.filter(visible=True)

    response = HttpResponse(content_type="text/plain")
    response["Content-Disposition"] = 'attachment; filename="events.rtf"'

    for event in events:
        response.write(
            "\n".join(
                [
                    event.start_time.strftime("%Y-%m-%d %I:%M %p"),
                    event.name,
                    event.description,
                    event.room.name,
                    ", ".join(
                        f'{p.attendee.get_display_name()}{" (m)" if p.is_moderator else ""}'
                        for p in event.participant_set.all()
                    ),
                ]
            )
            + "\n\n"
        )

    return response
