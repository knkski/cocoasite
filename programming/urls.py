from django.urls import path

from . import views

urlpatterns = [
    path("suggest/", views.suggest, name="programming_suggest"),
    path("calendar/", views.calendar, name="programming_calendar"),
    path("text/", views.text, name="programming_text"),
    path("<event_id>/volunteer/", views.volunteer, name="programming_volunteer"),
    path("<event_id>/", views.details, name="programming_details"),
    path("", views.index, name="programming_index"),
]
