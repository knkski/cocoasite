import textwrap
from datetime import timedelta

import arrow
from colorful.fields import RGBColorField
from django.db import models

from convention.models import BaseModel, Constance


class Room(BaseModel):
    """A room that an event is held in."""

    constance = models.ForeignKey(Constance, on_delete=models.CASCADE)

    name = models.CharField(
        max_length=255,
        help_text="The name of this room.",
    )

    short_name = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        help_text="A shortened version of this room's name, suitable for badge labels.",
    )

    # Only currently used in the calendar field. Default to something rather neutral, that both
    # black and white text will show up on
    color = RGBColorField(default="#777777")

    important = models.BooleanField(
        default=True,
        help_text="""
    If False, this room will get collapsed into an "Other" category when space is tight.
    """.strip(),
    )

    def get_active_events(self, day=None):
        events = self.event_set.filter(
            room__isnull=False, start_time__isnull=False, visible=True
        ).order_by("start_time")

        if day:
            start, end = arrow.get(day).span("day")
            start = start.shift(hours=6)
            end = end.shift(hours=6)
            events = events.filter(start_time__gte=start.datetime, start_time__lte=end.datetime)

        return events

    def get_short_name(self):
        """Gets short name if set, otherwise gets regular name"""

        return self.short_name or self.name


class Format(BaseModel):
    """Represents the format of an event, such as Panel or Interview."""

    constance = models.ForeignKey(Constance, on_delete=models.CASCADE)

    name = models.CharField(max_length=255)

    color = RGBColorField()

    class Meta:
        ordering = ["name"]


class Category(BaseModel):
    """Represents the format of an event, such as Literary or Fannish."""

    constance = models.ForeignKey(Constance, on_delete=models.CASCADE)

    name = models.CharField(max_length=255)

    color = RGBColorField()

    class Meta:
        ordering = ["name"]
        verbose_name_plural = "Categories"


class Event(BaseModel):
    """Represents an event at the convention."""

    constance = models.ForeignKey(Constance, on_delete=models.CASCADE)

    name = models.CharField(
        max_length=255, help_text="The name of this event. Should be something short and catchy."
    )

    description = models.TextField(
        blank=True,
        default="",
        help_text="The description for this event. Can be longer and have multiple paragraphs.",
    )

    proposer = models.ForeignKey(
        "website.User",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        help_text=textwrap.dedent(
            """
            The user that proposed this event via the public site.
            If unset, this event was added via the admin site
        """
        ),
    )

    visible = models.BooleanField(
        default=False,
        help_text="Whether or not this event will be displayed on the public site",
    )

    # Event notes. These will not be displayed publicly
    notes = models.TextField(
        blank=True,
        default="",
        help_text="Administrator notes for this event",
    )

    room = models.ForeignKey(
        Room,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        help_text="The room that this event will be held in",
    )

    start_time = models.DateTimeField(
        blank=True,
        null=True,
        help_text="When this event starts",
    )

    duration = models.IntegerField(
        default=60,
        help_text="Event duration (in minutes)",
    )

    format = models.ForeignKey(
        Format,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        help_text="The format of this event",
    )

    category = models.ForeignKey(
        Category,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        help_text="The category (or track) of this event",
    )

    participants = models.ManyToManyField(
        "registration.Attendee",
        through="Participant",
        blank=True,
        help_text="The list of participants in this event",
    )

    attendance_size = models.IntegerField(
        blank=True,
        null=True,
        help_text="""
    The number of people that attended this event. Useful for tracking historical data.
    """.strip(),
    )

    def get_end_time(self):
        """Calculates end time from start time and duration."""
        if self.start_time:
            return self.start_time + timedelta(minutes=self.duration)

    def get_room_name(self):
        if self.room:
            return self.room.name

    class Meta:
        ordering = ["name"]

    @classmethod
    def get_active(cls):
        return cls.objects.filter(
            room__isnull=False, start_time__isnull=False, visible=True
        ).order_by("start_time")


class Participant(BaseModel):
    constance = models.ForeignKey(Constance, on_delete=models.CASCADE)

    event = models.ForeignKey(
        Event,
        on_delete=models.CASCADE,
        help_text="The event this participant is associated with",
    )

    attendee = models.ForeignKey(
        "registration.Attendee",
        on_delete=models.CASCADE,
        help_text="The registered person attending this event",
    )

    is_moderator = models.BooleanField(
        default=False,
        help_text="Whether or not this participant is moderating the event",
    )

    confirmed = models.BooleanField(
        default=False,
        help_text="Whether or not this participant is actually confirmed for the event",
    )

    notes = models.TextField(
        default="",
        blank=True,
        null=False,
        help_text="Notes about this participant",
    )

    class Meta:
        ordering = ["-is_moderator", "attendee__email"]

    def __str__(self):
        return f"<Participant: {self.attendee.email} -> {self.event.name}>"

    __repr__ = __str__
