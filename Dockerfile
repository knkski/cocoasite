FROM python:3.10

RUN apt-get update && \
    apt-get install -y --no-install-recommends postgresql-client && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app
COPY pyproject.toml poetry.lock ./
RUN pip install poetry
RUN poetry config virtualenvs.create false
RUN poetry install --no-dev

# Copy code over
COPY cocoa cocoa/
COPY convention convention/
COPY manage.py manage.py
COPY programming/ programming/
COPY registration/ registration/
COPY static/ static/
COPY templates/ templates/
COPY website website/

RUN python manage.py collectstatic --noinput

COPY uwsgi.ini .

ARG VERSION
RUN echo $VERSION > VERSION

CMD ["uwsgi", "--ini", "uwsgi.ini"]
