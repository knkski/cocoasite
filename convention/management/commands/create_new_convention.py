import argparse
import colorsys
import random
import string
from datetime import datetime

from django.core.management.base import BaseCommand

from convention.models import Constance, Convention
from programming.models import Category, Format
from website.models import User


class Command(BaseCommand):
    help = "Creates a convention"

    @staticmethod
    def valid_date(date):
        try:
            return datetime.strptime(date, "%Y-%m-%d")
        except ValueError:
            msg = "Not a valid date: '{0}'.".format(date)
            raise argparse.ArgumentTypeError(msg)

    def add_arguments(self, parser):
        parser.add_argument("name", type=str)
        parser.add_argument("--slug", required=True, type=str)

        parser.add_argument(
            "-s", "--start", required=True, type=self.valid_date, help="start date (YYYY-MM-DD)"
        )

        parser.add_argument("-e", "--end", required=True, help="end date (YYYY-MM-DD)")

    def handle(self, *args, **options):
        name = options["name"]
        slug = options["slug"]
        start_date = options["start"]
        end_date = options["end"]

        convention, _ = Convention.objects.get_or_create(name=name)

        constance = Constance.objects.create(
            convention=convention,
            name=name,
            slug=slug,
            description=f"{name} - an awesome convention!",
            start_date=start_date,
            end_date=end_date,
        )

        # Create a default superuser
        User.objects.create_superuser(
            convention=convention,
            email="admin@ksk.io",
            first_name="Kenneth",
            last_name="Koski",
            password="".join(
                random.choice(string.ascii_uppercase + string.digits) for _ in range(20)
            ),
        )

        # Generate types. Uses the golden ratio to generate semi-random pastel
        # colors that work well together.
        ratio = 0.618
        h = random.random()

        def get_random_color():
            nonlocal h
            h = h + ratio % 1
            r, g, b = colorsys.hsv_to_rgb(h, 0.3, 0.99)

            return f"#{int(255*r):02x}{int(255*g):02x}{int(255*b):02x}"

        Format.objects.bulk_create(
            [
                Format(constance=constance, name=name, color=get_random_color())
                for name in [
                    "Panel",
                    "Interview",
                    "Reading",
                    "Audiovisual",
                    "Performance",
                    "Ceremony",
                    "Signing",
                    "Klatsch",
                    "Participatory",
                    "Film",
                ]
            ]
        )

        Category.objects.bulk_create(
            [
                Category(constance=constance, name=name, color=get_random_color())
                for name in [
                    "Literary",
                    "Science",
                    "Arts/Crafts",
                    "Miscellaneous",
                    "Traditional",
                    "Media",
                    "Writing",
                    "Music",
                    "Fannish/Zeitgeist",
                    "Gaming",
                    "Kids",
                    "Krushenko's Track",
                    "Film",
                ]
            ]
        )
