from django.core.management.base import BaseCommand

from website.models import User


class Command(BaseCommand):
    help = "Makes specified email address(es) admin user(s)"

    def add_arguments(self, parser):
        parser.add_argument("email", nargs="+")

    def handle(self, *args, **options):
        users = User.objects.filter(email__in=options["email"])

        found = set(u.email for u in users)
        not_found = set(options["email"]) - found

        if not_found:
            print("These emails not found:")
            print("\n".join(sorted(not_found)))

        users.update(is_staff=True, is_superuser=True)

        print(f"Updated {users.count()} users.")
