import colorsys
import random
from datetime import timedelta
from itertools import count

import pytz
from django.contrib.auth.hashers import make_password
from django.contrib.sites.models import Site
from django.core.management.base import BaseCommand
from django.utils.text import slugify
from faker import Faker
from taggit.models import Tag

from convention.models import Constance, Convention
from programming import models
from registration.models import Attendee
from website.models import User


def mostly(option):
    """Mostly returns what's passed in, but sometimes returns None.

    Useful for generating incomplete data.

    Returns True/False instead of None if the input is boolean
    """
    return option if random.random() < 0.9 else not option if option in [True, False] else None


class Command(BaseCommand):
    help = "Generates fake example data"

    @staticmethod
    def get_room_name(fake):
        die_roll = random.randint(0, 7)

        if die_roll == 0:
            num = fake.random_int(1, 20)
            return {"name": f"Veranda {num}", "short_name": f"Ver{num}"}
        elif die_roll == 1:
            prefix = fake.city_prefix()
            return {
                "name": f"{prefix} Ballroom",
                "short_name": prefix,
            }
        elif die_roll == 2:
            name = fake.last_name()
            return {
                "name": f"{name}'s",
                "short_name": f"{name[:3]}'s",
            }
        elif die_roll == 3:
            char = chr(65 + fake.random_int(0, 25))
            return {
                "name": f"Gaming {char}",
                "short_name": None,
            }
        elif die_roll == 4:
            city = fake.city()
            return {
                "name": f"{city} Room",
                "short_name": city[:5],
            }
        elif die_roll == 5:
            color = fake.safe_color_name().capitalize()
            return {
                "name": f"{color} Court",
                "short_name": color,
            }
        elif die_roll == 6:
            lounge = fake.word().capitalize()
            return {
                "name": f"{lounge} Lounge",
                "short_name": lounge,
            }
        else:
            latin_fake = Faker("la")
            word = latin_fake.word().capitalize()
            return {
                "name": f"Cinema {word}",
                "short_name": "FILM",
            }

    @staticmethod
    def get_random_colors(value=0.99):
        """Generates pastel colors.

        Uses the golden ratio to generate semi-random colors that work well together.
        """
        ratio = 0.618
        start = random.random()

        def to_rgb(i):
            r, g, b = colorsys.hsv_to_rgb(start + (i + 1) * ratio % 1, 0.3, value)
            return f"#{int(255 * r):02x}{int(255 * g):02x}{int(255 * b):02x}"

        yield from (to_rgb(i) for i in count())

    @staticmethod
    def weighted_choice(choices):
        total = sum(w for c, w in choices)
        r = random.uniform(0, total)
        upto = 0
        for c, w in choices:
            if upto + w >= r:
                return c
            upto += w
        assert False, "Shouldn't get here"

    def handle(self, *args, **options):
        fake = Faker()

        print("Generating Convention...")
        con_name = f"{fake.word().capitalize()}Con"
        first_date = fake.date_object()

        site = Site.objects.get()
        site.domain = f"{con_name}.devel"
        site.name = con_name
        site.save()

        convention = Convention.objects.create(name=con_name)

        print("Generating tags...")
        Tag.objects.bulk_create(
            [Tag(name=tag, slug=slugify(tag)) for tag in ["Guest of Honor", "Author"]]
        )

        print("Creating admin user...")
        # Create one easy-to-use fake admin user
        admin_user = User.objects.create_superuser(
            convention=convention,
            email="foo@bar.com",
            first_name=fake.first_name(),
            last_name=fake.last_name(),
            password="asdf1234",
        )

        Attendee(
            convention=convention,
            registrant=admin_user,
            email=admin_user.email,
            first_name=admin_user.first_name,
            last_name=admin_user.last_name,
        ).save()

        print("Creating normal user...")
        # Create one easy-to-use fake normal user
        normal_user = User.objects.create_user(
            convention=convention,
            email="bar@foo.com",
            first_name=fake.first_name(),
            last_name=fake.last_name(),
            password="asdf1234",
        )

        Attendee(
            convention=convention,
            registrant=normal_user,
            email=normal_user.email,
            first_name=normal_user.first_name,
            last_name=normal_user.last_name,
        ).save()

        print("Creating normal users...")
        users = User.objects.bulk_create(
            [
                User(
                    convention=convention,
                    email=fake.email(),
                    first_name=fake.first_name(),
                    last_name=fake.last_name(),
                    password=make_password("asdf1234"),
                    is_active=True,
                    is_staff=False,
                    is_superuser=False,
                )
                for _ in range(50)
            ]
        )

        Attendee.objects.bulk_create(
            [
                Attendee(
                    convention=convention,
                    registrant=user,
                    email=user.email,
                    first_name=user.first_name,
                    last_name=user.last_name,
                )
                for user in users
            ]
        )

        constance_count = 3
        for i in range(constance_count):
            print(f"Creating Constance {i+1}/{constance_count}...")
            start_date = first_date + timedelta(days=365 * i)
            end_date = start_date + timedelta(days=3)

            constance_name = f"{con_name} {i+1}"

            constance = Constance.objects.create(
                name=constance_name,
                convention=convention,
                slug=slugify(constance_name),
                description=f"{constance_name} is an awesome convention!",
                start_date=start_date,
                end_date=end_date,
            )

            print("Generating rooms...")
            room_colors = self.get_random_colors(value=0.8)
            models.Room.objects.bulk_create(
                [
                    models.Room(
                        **self.get_room_name(fake),
                        constance=constance,
                        color=next(room_colors),
                        important=mostly(True),
                    )
                    for _ in range(10)
                ]
            )

            rooms = constance.room_set.all()

            print("Generating formats...")
            format_colors = self.get_random_colors()
            models.Format.objects.bulk_create(
                [
                    models.Format(constance=constance, name=name, color=next(format_colors))
                    for name in [
                        "Panel",
                        "Music",
                        "Interview",
                        "Reading",
                        "Participatory",
                        "AudioVisual",
                    ]
                ]
            )

            formats = constance.format_set.all()

            print("Generating categories...")
            category_colors = self.get_random_colors()
            models.Category.objects.bulk_create(
                [
                    models.Category(constance=constance, name=name, color=next(category_colors))
                    for name in ["Literary", "Fannish", "Science", "Arts/Crafts", "Miscellaneous"]
                ]
            )

            categories = constance.category_set.all()

            print("Generating events...")

            def get_event():
                start_time = fake.date_time_between(
                    start_date=start_date, end_date=end_date, tzinfo=pytz.utc
                ).replace(
                    hour=fake.random_int(10, 25) % 23, minute=random.choice([0, 30]), second=0
                )

                choice = fake.random_int(0, 10)

                if choice == 0:
                    job = fake.job().lower()
                    format_name = "Panel"
                    event_name = f"Gaming while being a ‘{job}’?"
                    event_description = (
                        f"How does being a ‘{job}’ affect how you game? "
                        f"How does gaming affect how you do your work as a {job}?"
                    )
                elif choice == 1:
                    name = fake.name()
                    format_name = "Interview"
                    event_name = f"Bierklatsch with “{name}”"
                    event_description = (
                        f"In which we chat with “{name}” about anything and everything over a beer."
                    )
                elif choice == 2:
                    name = fake.name()
                    format_name = "Interview"
                    event_name = f"GoH Interview: {name}"
                    event_description = (
                        f"The moment we've all been waiting for. "
                        f"Grill {name} with all of your dumbest questions"
                    )
                elif choice == 3:
                    job = fake.job().lower()
                    format_name = "Panel"
                    event_name = f"Romancing alien {job}s"
                    event_description = (
                        f"Writers talk about their stories (safe for work and non-) about "
                        f"sex and romance with all sorts of creatures, but particularly of "
                        f"the {job} variety."
                    )
                elif choice == 4:
                    name = fake.name()
                    format_name = "Reading"
                    event_name = f"{name} reading"
                    event_description = (
                        f"In which we sit in silence and attempt to read {name}'s "
                        f"body language from afar."
                    )
                elif choice == 5:
                    genre = random.choice(["Fantasy", "Science Fiction"])
                    century = fake.century()
                    format_name = "Panel"
                    event_name = f"{genre} in the {century} century"
                    event_description = (
                        f"What is life like in the {century} century? "
                        f"More importantly, what is {genre} like?"
                    )
                elif choice == 6:
                    city = fake.city()
                    format_name = "Panel"
                    event_name = f"My journey to {city}"
                    event_description = (
                        f"{fake.name()} talks about their recent adventure to {city}, "
                        f"and how it changed their writing style."
                    )
                elif choice == 7:
                    sentence = fake.sentence()[:-1]
                    format_name = "Panel"
                    event_name = f"{sentence}: How? Why?"
                    event_description = (
                        f"Exactly what it says on the tin. Let's talk about {sentence}, "
                        f"the how and the why."
                    )
                elif choice == 8:
                    name = fake.name()
                    format_name = "Music"
                    event_name = f"{name} concert"
                    event_description = f"Come see {name} play their favorite instrument."
                elif choice == 9:
                    name = fake.name()
                    format_name = "Interview"
                    event_name = f"Kaffeeklatsch with {name}"
                    event_description = f"Talk with {name} over a coffee or five."
                else:
                    name = fake.name()
                    format_name = "Interview"
                    event_name = f"Skoomaklatsch with {name}"
                    event_description = f"Relax with {name} and some moon sugar."

                return models.Event(
                    constance=constance,
                    name=event_name,
                    description=event_description,
                    visible=random.random() < 0.9,
                    room=mostly(random.choice(rooms)),
                    format=mostly(next(f for f in formats if f.name == format_name)),
                    category=mostly(random.choice(categories)),
                    start_time=mostly(start_time),
                )

            events = models.Event.objects.bulk_create(
                [get_event() for _ in range(fake.random_int(120, 140))]
            )

            print("Generating attendees...")
            attendees = [(a, random.randint(1, 10)) for a in Attendee.objects.all()]

            def get_hosts(event):
                moderators = [
                    models.Participant(
                        constance=constance,
                        event=event,
                        attendee=self.weighted_choice(attendees),
                        is_moderator=True,
                        confirmed=True,
                    )
                ]

                other = [
                    models.Participant(
                        constance=constance,
                        event=event,
                        attendee=self.weighted_choice(attendees),
                        is_moderator=False,
                        confirmed=mostly(True),
                    )
                    for _ in range(fake.random_int(2, 4))
                ]

                return moderators + other

            models.Participant.objects.bulk_create([host for e in events for host in get_hosts(e)])

        print("DONE!")
