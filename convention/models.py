import uuid
from datetime import time

from django.db import models
from shortuuidfield import ShortUUIDField
from timezone_field import TimeZoneField


class BaseModel(models.Model):
    """Handles adding some common fields to each table.

    Also defines common convenience functions
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    uuid = ShortUUIDField()

    created_at = models.DateTimeField(auto_now_add=True)

    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    def __str__(self):
        # Most models have a name attribute, otherwise default to Django's
        # default __str__ method
        if hasattr(self, "name") and hasattr(self, "constance"):
            return f"{self.__class__.__name__} - {self.name} ({self.constance.slug})"
        elif hasattr(self, "name"):
            return f"{self.__class__.__name__} - {self.name}"
        else:
            return super().__str__()

    def __repr__(self):
        # Most models have a name attribute, otherwise default to Django's
        # default __repr__ method
        if hasattr(self, "name") and hasattr(self, "constance"):
            return f"<{self.__class__.__name__}: {self.name} ({self.constance.slug})>"
        elif hasattr(self, "name"):
            return f"<{self.__class__.__name__}: {self.name}>"
        else:
            return super().__repr__()


class Convention(BaseModel):
    """Represents an overarching convention that may have several instances

    TODO: There can only be one of these currently
    """

    # Convention name
    name = models.CharField(max_length=255)

    def current_constance(self):
        return self.constance_set.order_by("-start_date")[0]


class Constance(BaseModel):
    """Represents the details for a particular convention.

    Convention Instance -> Constance
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    convention = models.ForeignKey(Convention, on_delete=models.CASCADE)

    uuid = ShortUUIDField()

    # Convention name
    name = models.CharField(max_length=255)

    # Convention slug for URLs and whatnot
    slug = models.CharField(max_length=255, unique=True)

    # Convention description
    description = models.TextField()

    # Convention start date
    start_date = models.DateField()

    # Convention end date
    end_date = models.DateField()

    timezone = TimeZoneField(
        default="America/Chicago",
        help_text="Currently unused",
    )

    day_start = models.TimeField(
        default=time(6, 0),
        help_text="When to treat events as occurring near the beginning of the day instead of"
        " near the end of the previous",
    )

    def __str__(self):
        return f"{self.name} Convention"
