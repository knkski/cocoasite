from django.contrib import admin

from . import models


@admin.register(models.Constance)
class ConstanceAdmin(admin.ModelAdmin):
    fields = (
        "name",
        "slug",
        "description",
        "start_date",
        "end_date",
        "timezone",
        "day_start",
        "convention",
    )

    list_display = (
        "name",
        "slug",
        "start_date",
        "end_date",
        "timezone",
        "day_start",
    )

    list_filter = (
        "start_date",
        "end_date",
    )

    def get_changeform_initial_data(self, request):
        return {"convention": models.Convention.objects.get()}
