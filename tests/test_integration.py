"""Run Selenium tests."""

from datetime import datetime

import pytest
import trio
from django.conf import settings
from django.core.management import call_command
from selenium.webdriver.common.by import By
from selenium.webdriver.common.devtools.v85.runtime import ExceptionThrown
from selenium.webdriver.common.log import Log
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from convention.models import Convention

pytestmark = pytest.mark.django_db


@pytest.fixture(scope="function")
def cocoa_setup(django_db_setup, django_db_blocker):
    """Set up settings and database for each test."""
    settings.STATICFILES_STORAGE = "django.contrib.staticfiles.storage.StaticFilesStorage"
    settings.MIDDLEWARE = [mw for mw in settings.MIDDLEWARE if "whitenoise" not in mw]
    with django_db_blocker.unblock():
        call_command("generate_test_data")


@pytest.fixture
async def driver(request, nursery):
    """Set up Selenium driver."""
    options = Options()
    options.headless = not request.config.option.headful

    service = Service(log_path="selenium/geckodriver.log")

    with WebDriver(options=options, service=service) as driver:
        wait = WebDriverWait(driver, 30, 1)

        async with driver.bidi_connection() as session:
            log = Log(driver, session)
            await nursery.start(ensure_no_js_errors, log)

            yield driver, wait

        driver.get_screenshot_as_file(f"selenium/{request.node.name}.png")


async def ensure_no_js_errors(log, task_status):
    """Ensure no JS errors happen while browsing.

    Runs in background as async task, and calls pytest.fail if any errors are
    encountered.
    """
    session = log.cdp.get_session_context("page.enable")
    await session.execute(log.devtools.page.enable())
    session = log.cdp.get_session_context("runtime.enable")
    await session.execute(log.devtools.runtime.enable())
    receiver = session.listen(ExceptionThrown)
    async with receiver:
        task_status.started()
        async for value in receiver:
            pytest.fail(repr(value))


async def test_event_times(cocoa_setup, driver, live_server):
    """Test that event times are grouped and displayed properly."""
    driver, wait = driver

    run = trio.to_thread.run_sync

    await run(driver.get, live_server.url)
    rows = await run(driver.find_elements, By.CLASS_NAME, "list-group-item")
    ids = set(r.get_attribute("data-event-id") for r in rows)
    constance = Convention.objects.get().current_constance()
    events = constance.event_set.filter(visible=True)
    expected = set(str(e.id) for e in events)
    assert ids == expected

    events = {str(e.id): e for e in events}

    # Run through the grouped events, calculate what the date is displayed as,
    # and then assert that it matches event.start_time.
    dates = await run(driver.find_elements, By.CLASS_NAME, "event-list")
    for date_group in dates:
        date = (await run(date_group.find_element, By.XPATH, "./preceding-sibling::h3[1]")).text
        rows = await run(date_group.find_elements, By.CLASS_NAME, "list-group-item")
        current_time = None
        for row in rows:
            event = events[row.get_attribute("data-event-id")]
            time = (await run(row.find_element, By.CLASS_NAME, "time-column")).text
            if time:
                current_time = time

            if date == "Not yet scheduled":
                assert event.start_time is None
            else:
                parsed = datetime.strptime(
                    f"{date}/{current_time}",
                    "%A, %B %d/%H:%M",
                )
                parsed = parsed.replace(
                    year=constance.start_date.year,
                    tzinfo=event.start_time.tzinfo,
                )
                assert parsed == event.start_time


async def test_programming_page(cocoa_setup, driver, live_server):
    """Test that /programming/ loads properly."""
    driver, wait = driver

    run = trio.to_thread.run_sync

    await run(driver.get, live_server.url)
    button = await run(driver.find_element, By.CLASS_NAME, "toggle-expansion")
    await run(wait.until, EC.visibility_of(button))

    rows = await run(driver.find_elements, By.CLASS_NAME, "event-rows")
    assert len(rows) > 0

    filtering = await run(driver.find_element, By.CSS_SELECTOR, ".filter-events label")
    await run(filtering.click)

    event = await run(driver.find_element, By.CSS_SELECTOR, ".event-row .name a")
    await run(event.click)

    main = await run(driver.find_element, By.ID, "event-details-main")
    await run(wait.until, EC.visibility_of(main))


async def test_calendar_page(cocoa_setup, driver, live_server):
    """Test that /programming/calendar/ loads properly."""
    driver, wait = driver

    run = trio.to_thread.run_sync

    await run(driver.get, f"{live_server.url}/programming/calendar/")
    button = await run(driver.find_element, By.CLASS_NAME, "fc-center")
    await run(wait.until, EC.visibility_of(button))


async def test_reports_page(cocoa_setup, driver, live_server):
    """Test that /reports/ loads properly."""
    driver, wait = driver

    run = trio.to_thread.run_sync

    await run(driver.get, f"{live_server.url}/accounts/login/")
    username = await run(driver.find_element, By.ID, "id_login")
    password = await run(driver.find_element, By.ID, "id_password")
    submit = await run(driver.find_element, By.CSS_SELECTOR, "[type='submit']")

    username.send_keys("foo@bar.com")
    password.send_keys("asdf1234")
    await run(submit.click)

    button = await run(driver.find_element, By.CLASS_NAME, "toggle-expansion")
    await run(wait.until, EC.visibility_of(button))
    await run(driver.get, f"{live_server.url}/manage/reports/")

    button = await run(driver.find_element, By.ID, "email-report")
    await run(wait.until, EC.visibility_of(button))
    await run(button.click)
