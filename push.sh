#!/usr/bin/env fish

set -l TAG (git describe --always --dirty)

echo $TAG

docker build -t registry.gitlab.com/knkski/cocoasite:$TAG .
docker push registry.gitlab.com/knkski/cocoasite:$TAG

if not string match --regex '.*-dirty$' $TAG >/dev/null
    docker build -t registry.gitlab.com/knkski/cocoasite:latest .
    docker push registry.gitlab.com/knkski/cocoasite:latest
end
