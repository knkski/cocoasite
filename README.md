Cocoa
=====

Introduction
------------

TODO: Fill this out

Testing
-------

Install geckodriver:

    wget https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz
    tar -xzvf geckodriver-v0.24.0-linux64.tar.gz
    mv geckodriver ~/.local/bin/

Run tests:

    python manage.py test

Deploying
---------

Run `scripts/deploy-local.fish` for a Docker-based deploy, or `scripts/deploy.fish` for a hyper.sh-based deploy.

To create a new convention instance:

    hyper exec -it cocoasite-cocoa-web-1 python manage.py create_new_convention --slug minicon54 -s 2019-04-19 -e 2019-04-21 "Minicon 54"

To bring everything down, including volumes etc (WARNING DELETES DB VOLUME)
May be necessary if hyper.sh is doing weird things with referring to old images

    hyper compose down --rmi all -v --remove-orphans
